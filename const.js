// value of constant cannot be changed once it is declared,it remains constant,acts same as final in java

// constant
const pi=3.145;

// pi=100;(invalid)
console.log(pi);