function helloWorld(){
    console.log("hellow world");
}

//javascript fuction--> fucntion as well as object

//console.log(helloWorld.name)

//we can easily add new properties as we add in object
helloWorld.myProperty="very good property";
console.log(helloWorld.myProperty);

//prototype is the empty object {}
//only fuctions provide prototype property

console.log(helloWorld.prototype);

// we can add new properties in prototype
helloWorld.prototype.newProperty="abc";
helloWorld.prototype.sing=function(){
    return "sa re ga ma pa";
}
console.log(helloWorld.prototype);
console.log(helloWorld.prototype.sing());


const userMethods={
    about:function(){
        return `${this.firstName} is ${this.age} years old in age`
    },
    sing:function(){
        return "sa re ga ma";
    }
    
}

function createUser(firstName,lastName,age,gender,email){
    const user={};
    user.firstName=firstName;
    user.lastName=lastName;
    user.age=age;
    user.gender=gender;
    user.email=email;
    user.about=userMethods.about;
    user.sing=userMethods.sing;
}
//instead of creating new object for these functions we can add them to prototype

createUser.prototype.about=function(){
    return `${this.firstName} is ${this.age} years old in age`
}

createUser.prototype.sing=function(){
    return "sa re ga ma";
}