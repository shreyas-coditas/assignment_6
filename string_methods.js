
//trim()

let first_name="   shreyas  "

console.log(first_name);
first_name.trim();

console.log(first_name)//length remains same,bcs string is immutable,store it in other varible,or same one
let new_name = first_name.trim();
console.log(new_name.length);

first_name=first_name.trim();
console.log(first_name);

//toUpperCase
//changes the current sring to upper case

first_name=first_name.toUpperCase();
console.log(first_name);

//toLowerCase
//changes the current sring to lower case

first_name=first_name.toLowerCase();
console.log(first_name);

//slice()
//first index
//last index

//prints shre
let newName=first_name.slice(0,4);
console.log(newName);