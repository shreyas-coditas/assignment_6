//if we want to place rest of the parameters in any array

function myFuction(a,b,...c){
    console.log(a);//1
    console.log(b);//2
    console.log(c);//3,4,4,5,5,6,7

}

myFuction(1,2,3,4,4,5,5,6,7);

function addAll(...numbers){
    let sum=0;
    for(let number in numbers){
        sum+=number;
    }
    console.log(sum);
}

addAll(12,2,5,5,6,8,9,56);