//function that creates the object
//fn returns the object

function createUser(firstName,lastName,age,gender,email){
    const user={};
    user.firstName=firstName;
    user.lastName=lastName;
    user.age=age;
    user.gender=gender;
    user.email=email;

    user.about=function(){
        return `${this.firstName} is ${this.age} years old in age`
    };
}

const user1=createUser("shreyas","babshet",22,"male","dabkj@gmail.com");
console.log(user1);
const about=user1.about(); 


//we can also keep these fn in a sperate object

const userMethods={
    about:function(){
        return `${this.firstName} is ${this.age} years old in age`
    },
    sing:function(){
        return "sa re ga ma";
    }
    
}

function createUser(firstName,lastName,age,gender,email){
    const user={};
    user.firstName=firstName;
    user.lastName=lastName;
    user.age=age;
    user.gender=gender;
    user.email=email;
    user.about=userMethods.about;
    user.sing=userMethods.sing;
}