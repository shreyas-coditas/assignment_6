//callback fucntion

//we can pass fuction as the parameter to another function

function myFuction2(){
    console.log("inside function2");
}

function myFuction1(callback){
    console.log("inside fuction1");
    callback();//this is now the fuction2
}
myFuction1(myFuction2);


//fuction returning fucntion

function myFunc(){
    function hello(){
        console.log("Hellow world");
    }
    return hello;//returned the function hello
}
const ans=myFunc();//ans stores the function
ans();//we can use this ans to call the fn