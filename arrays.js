//arrays
//its the ordered collection of items
//array is objects

let fruits=["apple","mango","orange"];
console.log(fruits[1]);
console.log(fruits[0]);

//array can store numbers
let numbers=[1,2,3,,4,5];
console.log(numbers[3]);


//array can store any type of mixed values
let mixed=["string",1,2,3,4,null,undefined,"A"];
console.log(mixed);

//changing value by index
fruits[1]="banana";
console.log(fruits);

//checking type
console.log(typeof fruits);
console.log(Array.isArray(fruits));//true


//arrays push and pop

//push:pushes at the end
fruits.push("chikku");

//pop
//removes last element
let poppedFruit=fruits.pop();
console.log(poppedFruit);

//unshift and shift

//unshift: pushed element at the starting position
fruits.unshift("lemon");
fruits.unshift("kiwi");
console.log(fruits);

//shift:removes from the start
let remove=fruits.shift();
console.log(remove);

//push and pop are faster than shift and unshift


//creating array using const
//created on heap
const fruits=["apple","mango","orange"];
fruits.push("banana");
console.log(fruits);//["apple","mango","orange","bananna"]

const fruits=["apple","mango","orange"];
fruits=["banana"];
console.log(fruits);//invalid

