//Sets
//Iterable
//cannot be accessed using index
//no order is followed
//it does not store the duplicate values(only unique values)

const numbers=new Set([1,2,3])//syntax of set

console.log(numbers);

//sets can store any type of values
const items=["item1","item2"];
const number=new Set();
number.add(1);
number.add(2);
number.add(items);
number.add("abcd");
console.log(number);

// how to check presence of a number
if(number.has(1)){
    console.log("true");
}
else{
    console.log("false");
}


//iterating
for(let num in number){
    console.log(num);
}