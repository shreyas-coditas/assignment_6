//primitive and reference data types

//primitive:both the elements are alloted with the different space,with different address,they are completely different.
let num1=6;
let num2=num1;
console.log(num1);//6
console.log(num2);//6
num1++;
console.log(num1);//7
console.log(num2);//6

//reference types:both the array are pointing to the same address,and only one space is alloted on the heap
let array=["item1","item2"];
let array2=array;
console.log(array);//"item1","item2"
console.log(array2);//"item1","item2"

array2.push("item3");
console.log(array);//"item1","item2","item3"
console.log(array2);//"item1","item2","item3"


//different ways of making clone
// let array=["item1","item2"];
// let array2=["item1","item2"];
// console.log(array);//"item1","item2"
// console.log(array2);//"item1","item2"


//let array2=array.slice(0);
//let array2=[].concat(array);

// spread operator
let array3=[...array].concat(["item4","item5"]);

//adding more items after cloning
let array4=array.slice(0).concat(["item4","item5"]);