//storing objects inside array

const users=[
    {userId: 1,firstName: "Shreyas", gender: "male"},
    {userId: 2,firstName: "vinay", gender: "male"},
    {userId: 3,firstName: "suraj", gender: "male"},
];

console.log(users);

//for of loop

for(let user of users){
    console.log(user);
}
for(let user of users){
    console.log(user.firstName);
}
for(let user of users){
    console.log(user.userId);
}

//destructuring
const [user1,user2,user3]= user;
console.log(user2);

const [{firstName},{gender}]=user;
console.log(firstName);
console.log(gender);