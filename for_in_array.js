

//using for loops in array
let fruits=["apple","mango","orange"];
let fruits1=[];

for(let i=0;i<fruits.length-1;i++){
    console.log(fruits[i]);
    fruits1.push(fruits1[i].toUpperCase());
}

console.log(fruits1);

//using for of loop in arrays
const fruitss=["apple","mango","orange"];
for(let fruit of fruitss){
    console.log(fruit);
}

//using for in loop
const fruitses=["apple","mango","orange"];
for(let index in fruitses){
    console.log(fruit[index]);
}