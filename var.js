"use strict"


// defining variables

var firstname="Shreyas";
console.log(firstname);


// overwrites the previous value
firstname="Coder";
console.log(firstname);

//rules for variables
// var 1value=20;(invalid)
var value1=10;

console.log(value1);

// we can use only _ or $ in variables
var first_name="shreyas";
var last$name="babshet";

// spaces are invalid
// var first name="bablu";

// convensions
// use small letters and camalcase
firstName="shreyas";