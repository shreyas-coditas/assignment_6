//lexical scope:it is nothing but the local scope,any variable when needs to be fetched,it first check in its lexical scope,that is area whre the function is declared.

//after checking the current fuctions lexical scope,if not found it checks the outer fns lexical scope and so on...

const myVariable=20;

function app(){
    function myFunc(){
        const func=()=>{
            console.log("inside function",myVariable);
        }
    }

    console.log(myVariable);
    myFunc();
}

app();