const user1={
    first_name:"Mahesh",
    age:8,
    about:function(hobby){
        console.log(this.first_name,this.age);
    }
}

const user2={
    first_name:"Shreyas",
    age:20,
    
}

user1.about.call(user2);//it passes this-->user2,used to borrow about from user1
user1.about.call(user1);//this-->user1

user1.about.call(user2,"badminton");//we can pass the extra arguments also



//apply
//using apply we can pass the extra arguments in an array

about.apply(user1,["badminton"])//take extra as array


//Bind
//bind is similar to both but returns a fn

const func=about.bind(user2,"badminton");
func();

//it doesnt apply for arrow fuctions
//in arrow fn this represent one level outer then the current object,that is in above case window.
const user3={
    first_name:"Mahesh",
    age:8,
    about:()=>{
        console.log(this.first_name,this.age);
    }
}
user3.about(user1)//it takes this-->window.

//Even this works same
const user4={
    first_name:"Mahesh",
    age:8,
    about(){
        console.log(this.first_name,this.age);
    }
}