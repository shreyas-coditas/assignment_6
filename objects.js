//object
//objects are reference types
//objects store key value pairs
//objects dont have index


const aboutMe={name:"Shreyas",age:"22"};
console.log(aboutMe);

//accessing items
console.log(aboutMe.name);
console.log(aboutMe.age);


const person={
    age:22,
    name:"shreyas",
    hobbies: ["badminton","coding","trading"]
}
console.log(person);
console.log(person.hobbies);

//adding new value to object
person.gender="male";
//person[gender]="male";
console.log(person["gender"]);


console.log(person["name"]);


//using for in loop

for(let key in person){
    console.log(person[key]);
}

for(let key in person){
    console.log(`${key}:${person[key]}`);
    console.log(key,person[key]);
}

//getting the array of keys
console.log(Object.keys(person));


//Conputed properties
const key1="objkey1";
const key2="objkey2";

const val1="value1";
const val2="value2";

const object={
    [key1]:val1,
    [key2]:val2
}

// const obj={
//     obj[key1]:val1,
//     obj[key2]:val2
// }

//using spread in object
const obj1={
    key1:value1,
    key2:value2
}
const obj2={
    key3:value3,
    key4:value4
}
const newObject={...obj1,...obj2};
// const newObject={...obj1,...obj2,key89:"value89"};

const newObject1={..."abc"};
console.log(newObject);
console.log(newObject1);