
//using for loop

//with let
for(let i=0;i<100;i++){
    console.log(i);
}

//with var
for(var i=0;i<100;i++){
    console.log(i);
}
console.log(i);//we can use it outside the for loop,if we use var

//exmple of for
//sum of first n numbers
let sum=0;
let num=50;
for(let i=0;i<num;i++){
    sum=sum+i;
}
console.log(sum);

//break keyword
//when excutes the loop get cutoff and control comes out of loop