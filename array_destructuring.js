//array destructuring

let myvar=[1,2];
let [var1,var2]=myvar;
//const [var1,var2]=myvar;
console.log(var1,var2);

let myvariable=[1,2,4];
let [var3,,var4]=myvar;
console.log(var3,var4);//1,4


let [var5,var6,...myNewArray]=myvar;
console.log(var5,var6);
console.log(myNewArray);