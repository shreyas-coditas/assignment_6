

//block scope
//let and const are block scope
//var is fuction scope

{
    let number=10;
}
console.log(number);//displays as undefined
//they cannot be excessed out side the block

{
    let number1=10;
    console.log(number1);//prints 10;

}

{
    var number2=100;

}
console.log(number2);//prints 100,bcs var can be accesed in fn,bcs this is the main fuction it can be accesed.

function app(){
    if(true){
        var first_name="Shreyas";
        console.log(first_name);//can be printed
    }
    if(true){
        console.log(first_name);//can be printed
    }
}
app();
//but cannot be printed here