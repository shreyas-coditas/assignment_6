//using boolean comparison operaotors

let num1=100;
let num2=50;

console.log(num1<num2);//prints false

//==  and ===
let number1="8";
let number2=8;
console.log(number1==number2);//prints true,bcs == does not check data type,checks only values.

console.log(number1===number2);//prints false,checks data types along with values.


//!= and !==
//!= checks only values not data types
//!== checks data types also along with values


console.log(number1!=number2);//prints false,bcs == does not check data type,checks only values.

console.log(number1!==number2);//prints false,checks data types along with values.