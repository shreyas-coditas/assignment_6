//forEach

const numbers=[1,2,3,4,5,6,7,8];

function myFunc(number,index){
    console.log(number,index);
}

for(let i=0;i<numbers.length;i++){
    myFunc(numbers[i],i);
}
//shortest form to do this is using forEach

numbers.forEach(myFunc);//it performs the sam,passes value and its index as arguments

//we can also write the fucntion directly within th argument
numbers.forEach(function(number,index){
    console.log(`my number is ${number} and index is ${index}`);
})

//Map function(very important)

const numbers1=[10,20,30,40,50,60,70,80];
const square=function(number){
    return number*number;
}

const squaredNum=numbers1.map(square);//passes each number in square fn,and stores the squared values in array squaredNum.
console.log(square);//prints the array of squared numbers


// we can also write fn inside the map()
const sqrd=numbers1.map((number)=>{
    return number*number;
});

console.log(sqrd);


//filter :it uses the callback fn and returns true or false value

const numberss=[1,3,4,6,7,8,9];

const isEven=function(number){
    return number%2==0;
}

const evenNumbers=numbers.filter(isEven);
console.log(evenNumbers);//returns the array of even numbers

//we can also use array functions

const evenNumber=numbers.filter((numbers)=>{
    return number%2==0;
});
console.log(evenNumber);


//reduce fn:reduces and gives the single value

const sum=numberss.reduce((accumulator,currentValue)=>{
    return accumulator+currentValue;
});
// accumulator  currentValue return
// 1               3           4
// 4               4            8
// 8       ...so on

//real world example
const userCart=[
    {productId:1 ,productName:"mobile",price:1000},
    {productId:2 ,productName:"tv",price:20000},
    
]
const total=userCart.reduce((totalPrice,current)=>{
    return totalPrice+current.price;
},0);//totalPrice takes initial parameter as 0
console.log(total);

//Sort method
//in sorting sort mathod converts the array to string array and sorts it based on the ascii values

const numberArray=[100,500,457,67,78,23];
numberArray.sort();//the integers are not sorted

//we can sort integers as follows
numberArray.sort((a,b)=>a-b);

//sorting integers is as follows
// if a-b --->positive then return b,a;
//    a-b --->negetive then return a.b;


//find method
//find the first value only with condition
const myArray=["cat","dog","doggy","cow"];
const result=myArray.find((string)=>{
    return string.length===3;
});

//real example
const users=[
    {userId:1,username:"shreyas"},
    {userId:2,username:"shr"},
    {userId:3,username:"shrs"},
    {userId:4,username:"shreya"},
]
const myresult=users.find((user)=>{
    return user.userId==3;
})
console.log(myresult);


//every method:returns true if all the values satisfy condition

//callback fn returns --true/false
//every methond returns== true/false

const num=[2,4,6,8,10];
function isEven(number){
    return number%2===0;
}
const value=num.every(isEven);//isEven return true
console.log(value);//true


//real world example
const userCartt=[
    {productId:1 ,productName:"mobile",price:1000},
    {productId:2 ,productName:"tv",price:20000},
    
]

const res=userCart.every((product)=>product.price<30000);
console.log(res);//true


//some method
//returns true if there is any number which satisfies the condtion
const values=[1,2,3,5];
const answer=values.some((number)=>number%2==0);//true bcs 2 is even
console.log(answer);


//fill method
const array=new Array(10).fill(-1);//create array of size 10 and fill it with -1
const myArrayy=[1,2,3,4,5,6,7];
myArrayy.fill(0,2,5)//fill 0,from 2 to 5


//splice method
//start ,delete ,insert
const array1=["value1","value2","value3"];
array1.splice(1,0,"item to be inserted");//start index,num of items to be deleted,value to be inserted

console.log(array1);

const deletedItem=myArray.splice(1,2);
console.log(deletedItem);//both items are present


//insert and delete
const deleteItems=myArray.splice(1,2,"item1","item2");//starting from 1,delete 2 items,insert item1 and item2,
console.log(deleteItems);