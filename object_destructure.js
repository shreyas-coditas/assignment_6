

//object destructuring
const movie={
    movieName:"shole",
    songName:"gabber"
}
let movieName=movie.movieName;
let songName=movie.songName;

console.log(movieName,songName);

//or
//shortest form to destructure
const {movieName1,songName1}=movie;

//putting other properties in new object
const movie2={
    movieName:"shole",
    songName:"gabber",
    year:2015,
    rating:5
}
const {movieName2,songName2,...otherProperties}=movie2;
console.log(otherProperties);