//we can create any number of fuctions inside one fuction,using any number type(fn expression,arrow fn etc)

function app(){
    const myFuction=()=>{
        console.log("Printing Hellow World");
    }
    const addTwo=(number1,number2)=>{
        console.log(number1+number2);
    }
    const addThree=(number1,number2,number3)=>{
        console.log(number1+number2+number3);
    }
    myFuction();
    addTwo(1,2);
    addThree(3,4,5);
}
app();