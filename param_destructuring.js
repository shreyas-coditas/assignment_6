//parameter destructuring

//object
//we use in react

const user={
    userId:1234,
    first_name:"Shreyas",
    age:22

}


function details(obj){
    console.log(obj.first_name);
    console.log(obj.userId);
    console.log(obj.age);
}

//or
function printDetails({userId,first_name,age}){
    console.log(first_name);
    console.log(userId);
    console.log(age);
}