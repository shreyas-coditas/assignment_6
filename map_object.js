//Map object is different than map() method

// map stores the key value pairs
//the major diff in map and object is object stores keys as string or symbol,maps keys can be of ant type,

//in map we use set() and get()

const person=new Map();
person.set('firstName','Shreyas');
person.set('age',22);
console.log(person);
console.log(person.get(age));

console.log(person.keys());//prints set of keys

for(key in person.keys()){
    console.log(key);
}

for(key in person){
    console.log(key);//returns array of key and value

}

for(let [key,value] of person){
    console.log(key,value);
}

//we can also specify as
const personn=new Map(["firstname","shreyas"],["age",22]);


//creating object object pair
const person1={
    id:1,
    firstName:"shreyas"
}
const extra=new Map();
extra.set(person1,{age:10});
console.log(extra.get(person).age);