//undefined

let firstName;
console.log(firstName);//prints undefined

//null
let myVar=null;
console.log(myVar);//prints null
console.log(typeof myVar);//prints object,its a bug

//BigInt
//we can specify it as follows
let myNum=BigInt(12313131313122345345);
let myNumber=135n;

console.log(myNum,myNumber);