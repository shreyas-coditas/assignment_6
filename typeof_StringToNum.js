
//primitive data type
// String
// Number
// booleans
// undefined
// null
// BigInt
// symbol

//typeof()

let age=20;
let name="shreyas";

console.log(typeof age);//prints number
console.log(typeof name);//prints string

//convert string to number
let mynum= +"22";
console.log(mynum);
//can do this also

mynum=String(mynum);
console.log(typeof mynum);


//convert number to string
age=age+"";
console.log(typeof age);//string

//can do this
age=Number(age);
