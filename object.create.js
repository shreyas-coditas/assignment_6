//if we want to use the values of obj 1 in obj2


const obj1={
    key1:"value1",
    key2:"value2"
}

const obj2=Object.create(obj1);//we can now use values of obj1 also

obj2.key3="value3";
console.log(obj2);